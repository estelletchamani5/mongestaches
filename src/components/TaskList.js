import React, { useState } from "react";
import { FiEdit } from "react-icons/fi"

const Task = ({ task, removeTask, findItem, finishTask }) => {
  return (
    <li className="mt-3">
      <div className="row"> 
      <div className="col-4">
        <span>{task.title} </span>
      </div>
      <div className="col-3">
        <span>{task.statut} </span>
      </div>
      <div className="col-5">
        <button className="btn btn-success me-3 ms-3" title="supprimer" onClick={() => removeTask(task.id)}        >
          <i className="fas fa-trash-alt"></i>
        </button>{' '}
        <button className="btn btn-success"  onClick={() => findItem(task.id)}>
          <FiEdit />
        </button>
        <button className="btn btn-success ms-3" title="statut" onClick={() => finishTask(task.id)}>
         Terminé la tache
        </button>
      </div>
      </div>
    </li>
  )
}





const TaskList = ({ tasks = [], removeTask, findItem, finishTask }) => {
  const searchhandleChange = (e) => {
    let recherche = e.target.value
    setrecherche(
      recherche
    )
    console.log(recherche)
  }
  const [recherche, setrecherche] = useState("")
  let filtertem = recherche !== "" ? tasks.filter(item => (item.title.toLowerCase().includes(recherche.toLowerCase()))) : tasks
  return (

    <div>
      <input className='form-control w-50 mt-5' type="text" placeholder="Recherche" onChange={searchhandleChange} />
      {filtertem.length ? (
        <ul className="list">
          {filtertem.map(task => {
            return <Task
              task={task}
              key={task.id}
              removeTask={removeTask}
              findItem={findItem} 
              finishTask = {finishTask}
              />;
          })}
        </ul>
      ) : (
          <div className="no-tasks mt-5">No Tasks</div>
        )}
    </div>
  );
};

export default TaskList;
