import TaskList from "./TaskList";
import React, { useState, useEffect } from 'react'
import uuid from 'uuid'
import "../App.css";
import 'bootstrap/dist/css/bootstrap.css'




const App = () => {
  const [tasks, setTasks] = useState([])
  const [editItem, setEditItem] = useState(null)
  // Add tasks
  const addTask = title => {

    setTasks([...tasks, { title, id: uuid(), statut: "en cours" }])
  }
  // Remove tasks
  const removeTask = id => {
    setTasks(tasks.filter(task => task.id !== id))
  }

  // Clear tasks
  const clearList = () => {
    setTasks([])
  }

  //Find task
  const findItem = id => {
    const item = tasks.find(task => task.id === id)

    setEditItem(item)
  }

  // Edit task
  const editTask = (title, id) => {
    const newTasks = tasks.map(task => (task.id === id ? { title, id, statut : task.statut } : task))

    console.log(newTasks)

    setTasks(newTasks)
    setEditItem(null)
  }

  const finishTask = ( id) => {
    const newTasks = tasks.map(task => (task.id === id ?  { title: task.title, id, statut: "Terminé" } : task))

    console.log(newTasks)

    setTasks(newTasks)
  }





  const [title, setTitle] = useState('')

  const handleSubmit = e => {

    e.preventDefault()
    if (!editItem) {
      addTask(title)
      setTitle('')
    } else {
      editTask(title, editItem.id)
    }
  }

  const handleChange = e => {
    setTitle(e.target.value)
  }

  useEffect(() => {
    if (editItem) {
      setTitle(editItem.title)
      console.log(editItem)
    } else {
      setTitle('')
    }
  }, [editItem])

  return (
    // <TaskListContextProvider>
    <div className="container  mt-5 ">
      <div className="row  justify-content-center  ">
        <div className="col-7">
        <div className="card">
        <div class="card-body">
        <div className='header'>
      <h2 className="text-center">GESTIONNAIRE DE TACHE</h2>
    </div>
        <div className="main">
          <form onSubmit={handleSubmit} className="form">
            <input className='form-control mt-5 w-50' type="text" placeholder="Ajouter une tache" value={title} onChange={handleChange} required />
            <div className="mt-3">
            <button type="submit"  class="btn btn-success me-4" >
                {editItem ? 'Modifier la tache' : 'Ajouter'}
                </button>
              <button className="btn btn-success" title="clear" onClick={clearList}>
               supprimer tout
              </button>
            </div>
          </form>
          <TaskList
            tasks={tasks}
            removeTask={removeTask}
            findItem={findItem}
            finishTask = {finishTask}
          />
        </div>
      </div>
        
        </div>
      </div>
      
      </div>
    </div>
    // </TaskListContextProvider>
  );
};

export default App;
